﻿using Mic.Lesson.Figure.Shapes;
using Mic.Lesson.Figure.Shapes.Lines;
using Mic.Lesson.Figure.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape shape = new Rectangle();
            shape.Width = 20;
            shape.Height = 2;
            shape.Color = ConsoleColor.Yellow;

            //shape.Draw();
            Print(shape);
            Console.WriteLine();

            Table table = new Table
            {
                Column = 5,
                Row = 10,
                Color = ConsoleColor.Red
            };

            //table.Draw();
            Print(table);

            Console.ReadLine();
        }

        static IDrawable[] GetDrawables()
        {
            IDrawable[] arr =
            {
                new Table { Color = ConsoleColor.Yellow, Column = 3, Row = 5 },
                new Table { Color = ConsoleColor.Blue, Column = 3, Row = 5 },
                new Rectangle { Height = 5, Width = 20 }
            };

            return arr;
        }

        static void Print(IDrawable drawable)
        {
            string header = $"########## {drawable.GetType().Name} ##########";
            Console.WriteLine(header);
            Console.WriteLine();

            Console.ForegroundColor = drawable.Color;
            drawable.Draw();
            Console.ResetColor();

            Console.WriteLine(new string('#', header.Length));
        }

        //static void Print(Shape shape)
        //{
        //    string header = $"########## {shape.GetType().Name} ##########";
        //    Console.WriteLine(header);
        //    Console.WriteLine();

        //    shape.Draw();

        //    Console.WriteLine(new string('#', header.Length));
        //}

        //static void Print(Table shape)
        //{
        //    string header = $"########## {shape.GetType().Name} ##########";
        //    Console.WriteLine(header);
        //    Console.WriteLine();

        //    shape.Draw();

        //    Console.WriteLine();
        //    Console.WriteLine(new string('#', header.Length));
        //}
    }
}
