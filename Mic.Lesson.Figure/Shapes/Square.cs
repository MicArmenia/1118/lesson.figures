﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure.Shapes
{
    class Square : Rectangle
    {
        private byte _height;
        public sealed override byte Height
        {
            get => _height;
            set
            {
                _height = value;
                _width = value;
            }
        }

        private byte _width;
        public sealed override byte Width
        {
            get => _width;
            set
            {
                _width = value;
                _height = value;
            }
        }
    }
}
