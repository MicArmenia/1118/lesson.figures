﻿using System;

namespace Mic.Lesson.Figure.Shapes
{
    abstract class Shape : IDrawable
    {
        public Shape()
        {
            Color = Console.ForegroundColor;
        }

        public virtual byte Width { get; set; }
        public virtual byte Height { get; set; }
        public virtual ConsoleColor Color { get; set; }

        public abstract int Area();

        public abstract void Draw();
    }
}
