﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure.Shapes
{
    class Rectangle : Shape
    {
        public override int Area()
        {
            return Width * Height;
        }

        public override void Draw()
        {
            for (int i = 0; i < Height; i++)
            {
                Console.WriteLine(new string('*', Width));
            }
        }
    }
}